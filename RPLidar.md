Data Visualization (Walter Rafelsberger)
========================================

*Geschrieben von Franz Innerbichler1810837297, 3.1.2020*

(Umwandlung von Word in Markdown: pandoc -s example30.docx -t markdown
-o example35.md)

Aquire
------

Der RPLIDAR liefert 3 Möglichkeiten, Daten zu generieren:

1)  Ultra-simple,

2)  simple-grabber und

3)  mittels GUI.

Gleich vorweg, das Anmelden über die GUI hat bei mir nicht funktioniert
– deshalb hier nur eine Beschreibung von „ultra-simple“ und
„simple-grabber“.

RPLIDAR beschreibt in der Quick Start Karte die Anleitung zum Download
aller notwendigen Dateien. Folgende Visual Studio Projekte sind
verfügbar:

![](VS.png)

Nach Erstellen der jeweiligen Projekt-.exe können die Rohdaten in ein
File geschrieben werden (hier am Beispiel simple_grabber): Der USP Name
(?) „com4“ muss mitgeliefert werden, weil sonst die Fehlermeldung kommt,
dass com3 nicht verfügbar ist;

.\simple_grabber.exe com4 > .\out_simple.txt

Ultra_simple liefert die Rohdaten, solange der RPLidar läuft - nicht hilfreich, weil viele Umdrehungen die Datenbereinigung schwierig machen.

Simple_grabber liefert nach einem Header ein Histogramm der Daten und
nach einer Abfrage auch die Rohdaten (hier verkürzt dargestellt), aber nur eine Umdrehung:

RPLIDAR S/N: CF569AF2C1EA9FC0BEEB9CF340853200

Version: RPLidar health status : OK. (errorcode: 0)

waiting for data...

                                                     *                     
                                                     **                    
                         *                        *  **                    
                        **                        *  **                    
                 ***   ***                       *** **                    
                ****   *** **                    *** **      *             
              * ****   *** ***                  ***************            
              * *****  *** ****                ***************** *         
             ** *****  *** *****             ********************* **      
   **       *** ********** ********        *********************** *****   
*****      **** ********** *************************************** ********
*****     ***** ********** *************************************** ********
***** ** ***************** *************************************** ********
***** ************************************************************ ********
****************************************************************** ********
****************************************************************** ********
***************************************************************************
***************************************************************************
***************************************************************************
***************************************************************************

---------------------------------------------------------------------------

Do you want to see all the data? (y/n)

theta: 0.22 Dist: 00579.00

theta: 0.72 Dist: 00579.00

theta: 1.47 Dist: 00577.00

theta: 2.08 Dist: 00578.00

theta: 2.83 Dist: 00579.00

…

PARSE/FILTER
------------

Da das Histogramm nicht sehr aussagekräftig ist, wurde das C++ File
„main.cpp“ so umgeschrieben, dass nur mehr die Rohdaten ausgegeben
werden, allerdings bereits mit einem Komma als Separator, um das
Einlesen zu erleichtern (und alle anderen printf Befehle wurden
auskommentiert). Die Abfrage wurde belassen, um den Scan von 0-360° zu ermöglichen:

````c

// fetch exactly one 0-360 degrees' scan
    ans = drv->grabScanData(nodes, count);
    if (IS_OK(ans) || ans == RESULT_OPERATION_TIMEOUT) {
        drv->ascendScanData(nodes, count);
        // plot_histogram(nodes, count);
        printf("a,th,rad,di,dist\n");

        //printf("Do you want to see all the data? (y/n), \n");
        int key = getchar();
        if (key == 'Y' || key == 'y') {
            for (int pos = 0; pos < (int)count ; ++pos) {
                printf("%s, theta:, %03.2f, Dist:, %08.2f \n",(nodes[pos].sync_quality & RPLIDAR_RESP_MEASUREMENT_SYNCBIT) ?"S":" ", (nodes[pos].angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT)/64.0f, nodes[pos].distance_q2/4.0f);
            }
        }
} else {
printf("error code: %x\n", ans);
}
````

MINE/REPRESENT
--------------

Grundriss WC:

### Code in Python mit Matplotlib (beispielhaft):
````python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

wc_csv =
pd.read_csv("C:/Users/franz/source/repos/rplidar_sdk-release-v1.11.0/sdk/output/win32/Debug/out_simple_short.txt",
sep=",") #out_wc_clear

wc_csv2 = wc_csv.drop(['a', 'th','di'], axis=1)
wc_csv3 = wc_csv2[wc_csv2.dist != 0]
wc_csv4 = wc_csv3.dropna()
zpi = 2*np.pi/360
wc_csv4.rad = [float(i) * zpi for i in wc_csv4.rad]

# wc_csv4.loc[:,'rad'] = wc_csv4.loc[:,'rad']*zpi
# https://matplotlib.org/examples/pylab_examples/polar_demo.html

ax = plt.subplot(111, projection='polar')
ax.plot(wc_csv4.rad, wc_csv4.dist)
ax.set_theta_offset(np.pi)
ax.set_theta_zero_location("W")
plt.show()
````
![](matplotlib.png)

Darstellung wie erwartet: WC Abfluss und Klobürste hier bei 90°, bei 30° Spalt der Türe offen; bei 320° Bügelbrett, bei 260° kleiner Kasten

**Problem: spiegelverkeht, Achse 0-180**

## Wechsel zu R

### R code
````R
library(plotrix)

dat <- read.csv("C:/gitreps/out_simple_short.txt", sep=",")

# nur wichtige columns
dat2 <- dat[,c(3,5)]
#dat2[,1] <- as.numeric(levels(dat2[,1]))[dat2[,1]]
# dat2[,2] <- as.numeric(levels(dat2[,2]))[dat2[,2]]

dat3 <- dat2[!is.na(dat2[,1]),]
dat4 <- dat3[dat3[,2]!=0, ]
dat5 <- dat4[1:583,]

polar.plot(dat4[,2],dat4[,1],main="Test Polar Plot",lwd=3,line.col=4, rp.type="polygon")
````
![](R1.png)

**Problem: Skala von 600-1400mm verzerrt den Grundriss des WCs; daher
auf radial.plot gewechselt:**

````R
dat5[,1] <- dat5[,1]/360*2*pi # auf radians umrechnen

radial.plot(dat5[,2],dat5[,1],main="Grundriss WC",lwd=3,line.col=4, rp.type="polygon", radial.lim = c(0,1400))
````
![](R2.png)

**Problem: Spiegelverkehrt**
````R
radsort <- dat5[,1]
radsort2 <- radsort[order(radsort, decreasing = TRUE)]
dat6 <- dat5
dat6[,1] <- radsort2

radial.plot(dat6[,2],dat6[,1],main="Grundriss WC",lwd=3,line.col=4,
rp.type="polygon", radial.lim = c(0,1400))
````
![](R3.png)

**Um die Abstände und Distanzen besser ablesen zu können - drüberhovern:
plot.ly**

https://plot.ly/r/polar-chart/
````R
library(plotly)

p <- plot_ly(
type = 'scatterpolar',
mode = ’lines’,
r = dat4[,2],
theta = dat4[,1],
mode = 'markers'
)
p
````
![](Rplotly.png)
